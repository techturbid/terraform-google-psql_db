resource "google_sql_database_instance" "instance" {
  name             = var.name
  database_version = var.database_version
  region           = var.region

  depends_on = [
    google_service_networking_connection.connection
  ]

  settings {
    tier                        = var.tier
    activation_policy           = var.activation_policy
    availability_type           = var.availability_type
    authorized_gae_applications = []

    ip_configuration {
      ipv4_enabled = "false"
      private_network = "projects/${terraform.workspace}/global/networks/network"
      require_ssl = true
    }

    disk_autoresize = var.disk_autoresize
    disk_size       = var.disk_size
    disk_type       = var.disk_type
    pricing_plan    = var.pricing_plan
    user_labels     = {}

    location_preference {
      zone = var.zone
    }

    maintenance_window {
      day          = var.maintenance_window_day
      hour         = var.maintenance_window_hour
      update_track = "stable"
    }
  }

  timeouts {
    create = "10m"
    update = "10m"
    delete = "10m"
  }
}

//Required due to private instance
resource "google_compute_global_address" "address" {
  provider = "google-beta"
  name          = var.name
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  network       = "projects/${terraform.workspace}/global/networks/network"
}

resource "google_service_networking_connection" "connection" {
  provider      = "google-beta"
  network       = "projects/${terraform.workspace}/global/networks/network"
  service       = "servicenetworking.googleapis.com"
  # If you get a "Cannot modify allocated ranges in CreateConnection." you will need to
  # replace the reserved_peering_ranges with an empty array for the first run
  # See https://github.com/terraform-providers/terraform-provider-google/issues/3294
  # reserved_peering_ranges = []
  reserved_peering_ranges = [google_compute_global_address.address.name]
}

resource "google_sql_user" "users" {
  instance   = google_sql_database_instance.instance.name
  name       = var.name[count.index]
  password   = var.user_password
}

resource "google_sql_database" "databases" {
  instance   = google_sql_database_instance.instance.name
  count      = length(var.databases)
  name       = var.db_name[count.index]
  charset    = "UTF8"
  collation  = "en_US.UTF8"
}