variable "name" {
  type        = string
  description = "The name of the Cloud SQL resources"
  default     = "postgresql-db"
}

variable "database_version" {
  description = "The database version to use"
  default     = "POSTGRES_9_6"
}

variable "databases" {
  type = list(string)
}


variable "region" {
  description = "The region of the Cloud SQL resources"
  default     = "us-east4"
}

variable "zone" {
  description = "The zone for the master instance"
  default     = "us-east4-a"
}

variable "db_name" {
  description = "The name of the default database to create"
  type        = string
  default     = "default"
}

variable "users" {
  description = "The name of the default user"
  type        = string
  default     = "default"
}

variable "user_password" {
  description = "The password for the default user. If not set, a random one will be generated and available in the generated_user_password output variable."
  type        = string
  default     = ""
}

variable "tier" {
  description = "The tier for the master instance."
  # This is db-custom-{NUMBER_OF_CPUS}-{MEMORY_IN_MIB}
  # https://github.com/hashicorp/terraform/issues/12617#issuecomment-298617855
  default     = "db-custom-1-3840"
}

variable "activation_policy" {
  description = "The activation policy for the master instance.Can be either `ALWAYS`, `NEVER` or `ON_DEMAND`."
  default     = "ALWAYS"
}

variable "availability_type" {
  description = "The availability type for the master instance.This is only used to set up high availability for the PostgreSQL instance. Can be either `ZONAL` or `REGIONAL`."
  default     = "REGIONAL"
}

variable "disk_autoresize" {
  description = "Configuration to increase storage size."
  default     = true
}

variable "disk_size" {
  description = "The disk size for the master instance."
  default     = 60 //to start
}

variable "disk_type" {
  description = "The disk type for the master instance."
  default     = "PD_SSD"
}

variable "pricing_plan" {
  description = "The pricing plan for the master instance."
  default     = "PER_USE"
}

variable "maintenance_window_day" {
  description = "The day of week (1-7) for the master instance maintenance."
  default     = 1
}

variable "maintenance_window_hour" {
  description = "The hour of day (0-23) maintenance window for the master instance maintenance."
  default     = 23
}

